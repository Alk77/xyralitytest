import Foundation

protocol XTALoginProtocol {
	func completeLogin(error: Error?, result: [String]?)
}
