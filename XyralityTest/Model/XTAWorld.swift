import Foundation

// disclaimer: I know I can simply get value from the dictionary and don't create the whole not used class, but it's the right approach

class XTAWorld: NSObject {
	
		var country = ""
		var id = ""
		var language = ""
		var mapURL = ""
		var name = ""
		var url = ""
		var worldStatus : [String : Any] = [:]
	
	override init(){
		
	}
	
	init(dict : [String : Any]){
		self.country = 		dict["country"] as! String
		self.id = 			dict["id"] as! String
		self.language = 	dict["language"] as! String
		self.mapURL = 		dict["mapURL"] as! String
		self.name = 		dict["name"] as! String
		self.url = 			dict["url"] as! String
		self.worldStatus = 	dict["worldStatus"] as! [String : Any]
	}
}
