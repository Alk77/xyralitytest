import Foundation
import Alamofire

class XTALoginPresenter: NSObject {
	
	func verifyEmail(email: String) -> Bool {
		let placeholder = "^[a-z0-9A-Z]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
		let pattern = String(format: placeholder, arguments: [email])
		do {
			let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
			let results = regex.matches(in: email, options: [], range: NSRange(email.startIndex..., in: email))
			if results.count > 0 {
				return true
			}
			return false
		}
		catch _ {
			return false
		}
	}
	
	func verifyPassword(pwd: String) -> Bool {
		let placeholder = "^[0-9A-Za-z!@#$%_\\-]{6,20}$";
		let pattern = String(format: placeholder, arguments: [pwd])
		do {
			let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
			let results = regex.matches(in: pwd, options: [], range: NSRange(pwd.startIndex..., in: pwd))
			if results.count > 0 {
				return true
			}
			return false
		}
		catch _ {
			return false
		}
	}
	
	func login(view: XTALoginProtocol, email: String, password: String) {
		let parameters = ["login"		: email,
						  "password" 	: password,
						  "deviceType" 	: self.composeDeviceType(),
						  "deviceId" 	: self.composeDeviceId()]
		Alamofire.request(
			URL(string: "http://backend1.lordsandknights.com/XYRALITY/WebObjects/BKLoginServer.woa/wa/worlds")!,
			method: .post,
			parameters: parameters)
			.validate()
			.responsePropertyList { (response) in
				guard response.result.isSuccess else {
					view.completeLogin(error: response.result.error, result: nil)
					return
				}
				guard let value = response.result.value as? [String : Any],
					let rows = value["allAvailableWorlds"] as? [[String : Any]] else {
						view.completeLogin(error: response.result.error, result: nil)
						return
				}
				var worldsNameArray = [String]()
				for worldDict in rows {
					let world = XTAWorld.init(dict: worldDict)
					worldsNameArray.append(world.name)
				}
				view.completeLogin(error: nil, result: worldsNameArray)
		}
	}
	
	//MARK: service methods
	
	private func composeDeviceType() -> String {
		let deviceType = String(format: "%@ - %@ %@", arguments: [UIDevice.current.model, UIDevice.current.systemName, UIDevice.current.systemVersion])
		return deviceType
	}
	
	private func composeDeviceId() -> String {
		let deviceId = String(format: "%@", arguments: [UIDevice.current.identifierForVendor!.uuidString])
		return deviceId
	}
}
