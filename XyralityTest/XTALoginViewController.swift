import UIKit

class XTALoginViewController: UIViewController, UITextFieldDelegate  {
	
	var presenter : XTALoginPresenter!

	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var loginButton: UIButton!
	@IBOutlet weak var blurView: UIVisualEffectView!
	
	let toDataSegue = "fromLoginToDataSegue"
	var resultData = [String]()
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		self.presenter = XTALoginPresenter()
        tuneUI()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController?.setNavigationBarHidden(true, animated: true)
	}
	
	func tuneUI() {		
		self.emailTextField.attributedPlaceholder = NSAttributedString(string: "Email:", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
		self.passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password:", attributes: [NSAttributedStringKey.foregroundColor : UIColor.black])
		self.showBlur(show: false)
	}
	
	//MARK: text field delegates
	
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		textField.placeholder = ""
		return true
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField.tag < 2 {
			let nextTextField = self.view.viewWithTag(textField.tag + 1) as! UITextField
			nextTextField.becomeFirstResponder()
		}
		else {
			self.view.endEditing(true)
		}
		return true
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		if textField.tag == 1, textField.text == "" {
			textField.attributedPlaceholder = NSAttributedString(string: "Email:", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
		}
		if textField.tag == 2, textField.text == "" {
			textField.attributedPlaceholder = NSAttributedString(string: "Password:", attributes: [NSAttributedStringKey.foregroundColor : UIColor.black])
		}
	}

	//MARK: button handlers
	
	@IBAction func backgroundTapped(_ sender: UIControl) {
		self.view.endEditing(true)
	}
	
	@IBAction func loginPressed(_ sender: Any) {
		if !presenter.verifyEmail(email: emailTextField.text!) {
			let alert = XTAAlertHelper.shared.showAlert(title: "Wrong email!", message: "Please enter the correct email.")
			self.navigationController?.present(alert!, animated: true, completion: nil)
			return
		}
		if !presenter.verifyPassword(pwd: passwordTextField.text!) {
			let alert = XTAAlertHelper.shared.showAlert(title: "Wrong password!", message: "Password should contain from 6 to 20 symbols and can contain alphanumeric and !@#$%_- symbols")
			self.navigationController?.present(alert!, animated: true, completion: nil)
			return
		}
		self.showBlur(show: true)
		self.presenter.login(view: self, email: emailTextField.text!, password: passwordTextField.text!)		
	}
	
	//MARK: navigation
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == toDataSegue {
			let vc = segue.destination as! XTADataViewController
			vc.worlds = resultData
		}
	}
	
	//MARK: service functions
	
	func showBlur(show: Bool) {
		if (show) {
			self.emailTextField.isUserInteractionEnabled = false
			self.passwordTextField.isUserInteractionEnabled = false
			self.loginButton.isUserInteractionEnabled = false
			self.blurView.isHidden = false
		}
		else {
			self.emailTextField.isUserInteractionEnabled = true
			self.passwordTextField.isUserInteractionEnabled = true
			self.loginButton.isUserInteractionEnabled = true
			self.blurView.isHidden = true
		}
	}
	
}

//MARK: view protocol

extension XTALoginViewController : XTALoginProtocol {
	func completeLogin(error: Error?, result: [String]?) {
		self.showBlur(show: false)
		if error != nil {
			let alert = XTAAlertHelper.shared.showAlert(title: "Error", message: (error?.localizedDescription)!)
			self.navigationController?.present(alert!, animated: true, completion: nil)
		}
		else {
			resultData = result!
			self.performSegue(withIdentifier: toDataSegue, sender: self)
		}
	}
}
