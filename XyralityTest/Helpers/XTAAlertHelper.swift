import UIKit

final class XTAAlertHelper: NSObject {
	
	static let shared = XTAAlertHelper()
	var isShowAlertView = false
		
	func showAlert(title: String, message: String) ->UIAlertController? {
		if isShowAlertView {
			return nil
		}
		isShowAlertView = true
		
		let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let cancelAction = UIAlertAction(title: "OK", style: .cancel) { (UIAlertAction) in
			self.isShowAlertView = false
		}
		alertController.addAction(cancelAction)
		return alertController
	}

}
