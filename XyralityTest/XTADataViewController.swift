import UIKit


class XTADataViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

	@IBOutlet weak var tableView: UITableView!
	
	let cellIdentifier = "DataTableViewCell"
	var worlds = [String]()
	
    override func viewDidLoad() {
        super.viewDidLoad()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController?.setNavigationBarHidden(false, animated: true)
		self.navigationController?.navigationBar.barTintColor = UIColor(red: 233.0/255.0, green: 116.0/255.0, blue: 125.0/255.0, alpha: 1.0)
		self.navigationController?.navigationBar.isTranslucent = false;
		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
	}
	
	//MARK: table view delegates
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return worlds.count
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if UIDevice.current.userInterfaceIdiom == .phone {
			return 50
		}
		return 70
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! XTADataTableViewCell
		if indexPath.row % 2 == 0 {
			cell.backgroundColor = UIColor.gray
		}
		else {
			cell.backgroundColor = UIColor.white
		}
		cell.dataLabel.text = String(format: "%@", arguments: [worlds[indexPath.row]])
		return cell
	}
}
